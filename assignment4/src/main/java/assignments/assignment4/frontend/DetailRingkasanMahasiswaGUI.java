package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class DetailRingkasanMahasiswaGUI {

    // Membuat font baru
    public static Font fontRingkasan = new Font("Century Gothic", Font.BOLD , 14);

    public DetailRingkasanMahasiswaGUI(JFrame frame, Mahasiswa mahasiswa, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Membuat panel halaman ringkasan mahasiswa
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        // Membuat label judul halaman
        JLabel  judulLabel = new JLabel();
        judulLabel.setText("Detail Ringkasan Mahasiswa");
        judulLabel.setFont(SistemAkademikGUI.fontTitle);
        judulLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(Box.createHorizontalStrut(30)); // Sebagai jeda (jarak)
        panel.add(judulLabel); // Menambahkan label judul ke dalam panel
        
        // Membuat label nama
        JLabel namaLabel = new JLabel();
        namaLabel.setText(String.format("Nama: %s", mahasiswa.getNama()));
        namaLabel.setFont(SistemAkademikGUI.fontGeneral);
        namaLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(Box.createHorizontalStrut(20)); // Sebagai jeda
        panel.add(namaLabel); // Menambahkan label nama ke dalam panel
        
        // Membuat label npm
        JLabel npmLabel = new JLabel();
        npmLabel.setText(String.format("NPM: %d", mahasiswa.getNpm()));
        npmLabel.setFont(SistemAkademikGUI.fontGeneral);
        npmLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(Box.createHorizontalStrut(20));
        panel.add(npmLabel); // Menambahkan label npm ke dalam panel
        
        // Membuat label jurusan
        JLabel jurusanLabel = new JLabel();
        jurusanLabel.setText(String.format("Jurusan: %s", mahasiswa.getJurusan()));
        jurusanLabel.setFont(SistemAkademikGUI.fontGeneral);
        jurusanLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(Box.createHorizontalStrut(20));
        panel.add(jurusanLabel); // Menambahkan label jurusan ke dalam panel
        
        // Membuat label daftar matkul
        JLabel daftarLabel = new JLabel();
        daftarLabel.setText("Daftar Mata Kuliah:");
        daftarLabel.setFont(SistemAkademikGUI.fontGeneral);
        daftarLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(Box.createHorizontalStrut(20));
        panel.add(daftarLabel); // Menambahkan label daftar ke dalam panel

        // Conditional apabila belom terdapat matkul yang diambil
        if (mahasiswa.getBanyakMatkul() <= 0){
            // Membuat label matkul
            JLabel matkulKosongLabel = new JLabel();
            matkulKosongLabel.setText("Belum ada mata kuliah yang diambil");
            matkulKosongLabel.setFont(fontRingkasan);
            matkulKosongLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
            panel.add(Box.createVerticalStrut(20));
            panel.add(matkulKosongLabel); // Menambahkan label matkul ke dalam panel
        } else{
            // Iterasi untuk setiap matkul yang diambil
            for(int i = 0; i < mahasiswa.getMataKuliah().length; i++){
                if (mahasiswa.getMataKuliah()[i] != null){
                    // Membuat label matkul
                    JLabel matkulLabel = new JLabel();
                    matkulLabel.setText(String.format("%d. %s", i+1, mahasiswa.getMataKuliah()[i])); // Settext sesuai format
                    matkulLabel.setFont(fontRingkasan);
                    matkulLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
                    panel.add(Box.createVerticalStrut(20));
                    panel.add(matkulLabel); // Menambahkan label matkul ke dalam panel
                }
            }
        }

        // Membuat label total sks
        JLabel totalSksLabel = new JLabel();
        totalSksLabel.setText(String.format("Total SKS: %d", mahasiswa.getTotalSKS()));
        totalSksLabel.setFont(SistemAkademikGUI.fontGeneral);
        totalSksLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(Box.createVerticalStrut(20));
        panel.add(totalSksLabel); // Menambahkan label total sks ke dalam panel
        
        // Membuat label hasil irs
        JLabel hasilIRS = new JLabel();
        hasilIRS.setText("Hasi Pengecekan IRS:");
        hasilIRS.setFont(SistemAkademikGUI.fontGeneral);
        hasilIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(Box.createVerticalStrut(20));
        panel.add(hasilIRS); // Menambahkan label hasil irs ke dalam panel

        // Conditional apabila tidak ada masalah irs
        if (mahasiswa.getBanyakMasalahIRS() <= 0){
            // Membuat label irs
            JLabel irsLabel = new JLabel();
            irsLabel.setText("IRS tidak bermasalah");
            irsLabel.setFont(fontRingkasan);
            irsLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
            panel.add(Box.createVerticalStrut(20));
            panel.add(irsLabel); // Menambahkan label irs ke dalam panel
        } else{
            // Iterasi untuk setiap masalah IRS yang ada
            for(int i = 0; i < mahasiswa.getMasalahIRS().length; i++){
                if (mahasiswa.getMasalahIRS()[i] != null){
                    // Membuat label masalah
                    JLabel masalahLabel = new JLabel();
                    masalahLabel.setText(String.format("%d. %s", i+1, mahasiswa.getMasalahIRS()[i]));
                    masalahLabel.setFont(fontRingkasan);
                    masalahLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
                    panel.add(Box.createVerticalStrut(20));
                    panel.add(masalahLabel); // Menambahkan label masalah ke dalam panel
                }
            }
        }
        
        // Membuat button selesai untuk kembali ke halaman home
        JButton kembaliButton = new JButton();
        kembaliButton.setText("Selesai");
        kembaliButton.setFont(SistemAkademikGUI.fontGeneral);
        kembaliButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembaliButton.setBackground(Color.decode("#03C03C"));
        kembaliButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                panel.setVisible(false); // Set visibility panel menjadi false
                frame.remove(panel); // remove panel
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah); // Kembali ke halaman home
            }
        });
        panel.add(Box.createVerticalStrut(20));
        panel.add(kembaliButton); // Menambahkan button selesai ke dalam panel

        // Menambahkan panel ke dalam frame
        frame.add(panel);
        
    }
}
