package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMataKuliahGUI {

    private ArrayList<MataKuliah> daftarMataKuliah;

    public RingkasanMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        this.daftarMataKuliah = daftarMataKuliah;
        
        // Membuat panel halaman ringkasan matkul
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        // Membuat label judul halaman
        JLabel judulLabel = new JLabel();
        judulLabel.setText("Ringkasan Mata Kuliah");
        judulLabel.setFont(SistemAkademikGUI.fontTitle);
        judulLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Membuat label nama matkul
        JLabel namaLabel = new JLabel();
        namaLabel.setText("Pilih Nama Matkul");
        namaLabel.setFont(SistemAkademikGUI.fontGeneral);
        namaLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

         // Membuat list baru untuk setiap nama matkul
         ArrayList<String> listMatkul = new ArrayList<String>();
         for (int i = 0; i < daftarMataKuliah.size(); i++){
             listMatkul.add(daftarMataKuliah.get(i).getNama());
         }
         // Mengubah list menjadi array
         String[] arrayMatkul = new String[listMatkul.size()];
         arrayMatkul = listMatkul.toArray(arrayMatkul);
         
         // Sorting array
         for (int i  = 0; i < arrayMatkul.length; i ++){
             for (int j = i + 1; j < arrayMatkul.length; j ++){
                 int value = arrayMatkul[j].compareTo(arrayMatkul[i]);
                 if (value < 0){
                     String temp = arrayMatkul[i];
                     arrayMatkul[i] = arrayMatkul[j];
                     arrayMatkul[j] = temp;
                 }
             }
         }
        // Membuat Dropdown pilih matkul
        JComboBox<String> pilihMatkulComboBox = new JComboBox<>(arrayMatkul);
        pilihMatkulComboBox.setMaximumSize(new Dimension(200,30));
        
        // Membuat button lihat
        JButton lihatButton = new JButton();
        lihatButton.setText("Lihat");
        lihatButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        lihatButton.setFont(SistemAkademikGUI.fontGeneral);
        lihatButton.setBackground(Color.decode("#03C03C"));
        lihatButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                try{
                    // Set variabel
                    String nama = (String) pilihMatkulComboBox.getSelectedItem();       
                    panel.setVisible(false); // set visibility false
                    frame.remove(panel); // remove panel
                    new DetailRingkasanMataKuliahGUI(frame, getMataKuliah(nama), daftarMahasiswa, daftarMataKuliah); // menuju halaman detail ringkasan matkul
                } catch (NullPointerException npe){ // apabila terdapat field kosong
                    JOptionPane.showMessageDialog(null, "Mohon isi seluruh Field");
                }
            }
        });
        
        // Membuat button kembali 
        JButton kembaliButton = new JButton();
        kembaliButton.setText("Kembali");
        kembaliButton.setFont(SistemAkademikGUI.fontGeneral);
        kembaliButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembaliButton.setBackground(Color.decode("#7CB9E8"));
        kembaliButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                panel.setVisible(false); // Set visibility panel menjadi false
                frame.remove(panel); // remove panel
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah); // Kembali ke halaman home
            }
        });

        // Menambahkan label, field, dan button ke dalam panel
        panel.add(Box.createVerticalStrut(50));
        panel.add(judulLabel);
        panel.add(Box.createVerticalStrut(20));
        panel.add(namaLabel);
        panel.add(Box.createVerticalStrut(20));
        panel.add(pilihMatkulComboBox);
        panel.add(Box.createVerticalStrut(20));
        panel.add(lihatButton);
        panel.add(Box.createVerticalStrut(20));
        panel.add(kembaliButton);

        // Menambahkan panel ke dalam frame
        frame.add(panel);
    }

    // Method getter
    private MataKuliah getMataKuliah(String nama) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }
    
}
