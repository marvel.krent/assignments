package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import assignments.assignment4.backend.*;

public class HapusIRSGUI {

    // Atribut class 
    private ArrayList<Mahasiswa> daftarMahasiswa;
    private ArrayList<MataKuliah> daftarMataKuliah;

    public HapusIRSGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        this.daftarMahasiswa = daftarMahasiswa;
        this.daftarMataKuliah = daftarMataKuliah;

        // Membuat panel pada halaman hapus irs
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        // Membuat label judul halaman
        JLabel sambutan = new JLabel();
        sambutan.setText("Hapus IRS");
        sambutan.setAlignmentX(Component.CENTER_ALIGNMENT);
        sambutan.setFont(SistemAkademikGUI.fontTitle);

        // Membuat label pilih NPM
        JLabel pilihNPMLabel = new JLabel();
        pilihNPMLabel.setText("Pilih NPM");
        pilihNPMLabel.setFont(SistemAkademikGUI.fontGeneral);
        pilihNPMLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Membuat list baru untuk npm tiap mahasiswa
        ArrayList<Long> listNPM = new ArrayList<Long>();
        for (int i = 0; i < daftarMahasiswa.size(); i++){
            listNPM.add(daftarMahasiswa.get(i).getNpm());
        }
        // Mengubah list menjadi array
        Long[] arrayNPM = new Long[listNPM.size()];
        arrayNPM = listNPM.toArray(arrayNPM);

        // Sorting array
        for (int i  = 0; i < arrayNPM.length-1; i ++){
            for (int j = i + 1; j < arrayNPM.length; j ++){
                if (arrayNPM[i] > arrayNPM[j]){
                    long temp = arrayNPM[j];
                    arrayNPM[j] = arrayNPM[i];
                    arrayNPM[i] = temp;
                }
            }
        }
        // Membuat Dropdown pilih NPM
        JComboBox<Long> pilihNPMComboBox = new JComboBox<>(arrayNPM);
        pilihNPMComboBox.setMaximumSize(new Dimension(200,30));

        // Membuat label nama matkul
        JLabel pilihNamaLabel = new JLabel();
        pilihNamaLabel.setText("Pilih Nama Matkul");
        pilihNamaLabel.setFont(SistemAkademikGUI.fontGeneral);
        pilihNamaLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Membuat list baru untuk setiap nama matkul
        ArrayList<String> listMatkul = new ArrayList<String>();
        for (int i = 0; i < daftarMataKuliah.size(); i++){
            listMatkul.add(daftarMataKuliah.get(i).getNama());
        }
        // Mengubah list menjadi array
        String[] arrayMatkul = new String[listMatkul.size()];
        arrayMatkul = listMatkul.toArray(arrayMatkul);
        
        // Sorting array
        for (int i  = 0; i < arrayMatkul.length; i ++){
            for (int j = i + 1; j < arrayMatkul.length-1; j ++){
                int value = arrayMatkul[j-1].compareTo(arrayMatkul[j]);
                if (value > 0){
                    String temp = arrayMatkul[j-1];
                    arrayMatkul[j-1] = arrayMatkul[j];
                    arrayMatkul[j] = temp;
                }
            }
        }
        // Membuat Dropdown pilih matkul
        JComboBox<String> pilihMatkulComboBox = new JComboBox<>(arrayMatkul);
        pilihMatkulComboBox.setMaximumSize(new Dimension(200,30));
        
        // Membuat button hapus
        JButton hapusButton = new JButton();
        hapusButton.setText("Hapus");
        hapusButton.setFont(SistemAkademikGUI.fontGeneral);
        hapusButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        hapusButton.setBackground(Color.decode("#03C03C"));
        hapusButton.addActionListener(new ActionListener(){
            public void actionPerformed (ActionEvent e){
                try{
                    // Set variable
                    long npm = (long) pilihNPMComboBox.getSelectedItem();
                    String nama = (String) pilihMatkulComboBox.getSelectedItem();
                    // Menampilkan kotak dialog sesuai format
                    JOptionPane.showMessageDialog(null, getMahasiswa(npm).dropMatkul(getMataKuliah(nama)));

                } catch(NullPointerException npe){ // Apabila terdapat dropdown kosong
                    JOptionPane.showMessageDialog(null, "Mohon isi seluruh Field");
                }
            }
        });

        // Membuat button kembali
        JButton kembaliButton = new JButton();
        kembaliButton.setText("Kembali");
        kembaliButton.setFont(SistemAkademikGUI.fontGeneral);
        kembaliButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembaliButton.setBackground(Color.decode("#7CB9E8"));
        kembaliButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                panel.setVisible(false); // Set visibility panel menjadi false
                frame.remove(panel); // remove panel
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah); // Kembali ke halaman home
            }
        });

        // Menambahkan button, field, dan label ke dalam panel
        panel.add(Box.createVerticalStrut(50)); // Sebagai jeda
        panel.add(sambutan);
        panel.add(Box.createVerticalStrut(20));
        panel.add(pilihNPMLabel);
        panel.add(Box.createVerticalStrut(20));
        panel.add(pilihNPMComboBox);
        panel.add(Box.createVerticalStrut(20));
        panel.add(pilihNamaLabel);
        panel.add(Box.createVerticalStrut(20));
        panel.add(pilihMatkulComboBox);
        panel.add(Box.createVerticalStrut(20));
        panel.add(hapusButton);
        panel.add(Box.createVerticalStrut(20));
        panel.add(kembaliButton);

        // Menambahkan panel ke dalam frame
        frame.add(panel);
    }

    // Method getter
    private MataKuliah getMataKuliah(String nama) {

        for (MataKuliah mataKuliah : daftarMataKuliah) {
            if (mataKuliah.getNama().equals(nama)){
                return mataKuliah;
            }
        }
        return null;
    }
    private Mahasiswa getMahasiswa(long npm) {

        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }

}
