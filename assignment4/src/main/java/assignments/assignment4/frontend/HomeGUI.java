package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class HomeGUI {
    
    public HomeGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        
        // Membuat panel pada Home 
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        
        // Membuat Judul halaman Home
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Selamat datang di Sistem Akademik");
        titleLabel.setHorizontalAlignment(JLabel.CENTER);
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        // Membuat button tambah mahasiswa
        JButton tambahMahasiswa = new JButton();
        tambahMahasiswa.setText("Tambah Mahasiswa");
        tambahMahasiswa.setFont(SistemAkademikGUI.fontTitle);
        tambahMahasiswa.setAlignmentX(Component.CENTER_ALIGNMENT);
        tambahMahasiswa.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                panel.setVisible(false); // set visible false
                frame.remove(panel); // remove panel
                new TambahMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah); // Menuju halaman tambah mahasiswa
            }
        });
        
        // Membuat button tambah matkul
        JButton tambahMatkul = new JButton();
        tambahMatkul.setText("Tambah Mata Kuliah");
        tambahMatkul.setFont(SistemAkademikGUI.fontTitle);
        tambahMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        tambahMatkul.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e ){
                panel.setVisible(false); // Set visible false
                frame.remove(panel); // remove panel
                new TambahMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah); // Menuju halaman tambah mata kuliah
            }
        });
        
        // Membuat button tambah IRS
        JButton tambahIRS = new JButton();
        tambahIRS.setText("Tambah IRS");
        tambahIRS.setFont(SistemAkademikGUI.fontTitle);
        tambahIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        tambahIRS.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e ){
                panel.setVisible(false); // Set visible false
                frame.remove(panel); // remove panel
                new TambahIRSGUI(frame, daftarMahasiswa, daftarMataKuliah); // Menuju halaman tambah IRS
            }
        });
        
        // Membuat button hapus IRS
        JButton hapusIRS = new JButton();
        hapusIRS.setText("Hapus IRS");
        hapusIRS.setFont(SistemAkademikGUI.fontTitle);
        hapusIRS.setAlignmentX(Component.CENTER_ALIGNMENT);
        hapusIRS.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e ){
                panel.setVisible(false); // Set visible false
                frame.remove(panel); // remove panel
                new HapusIRSGUI(frame, daftarMahasiswa, daftarMataKuliah); // Menuju halaman hapus IRS
            }
        });
        
        // Membuat button ringkasan mahasiswa
        JButton ringkasanMhs = new JButton();
        ringkasanMhs.setText("Ringkasan Mahasiswa");
        ringkasanMhs.setFont(SistemAkademikGUI.fontTitle);
        ringkasanMhs.setAlignmentX(Component.CENTER_ALIGNMENT);
        ringkasanMhs.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e ){
                panel.setVisible(false); // Set visible false
                frame.remove(panel); // remove panel
                new RingkasanMahasiswaGUI(frame, daftarMahasiswa, daftarMataKuliah); // Menuju halaman ringkasan mahasiswa
            }
        });
        
        // Membuat button ringkasan matkul
        JButton ringkasanMatkul = new JButton();
        ringkasanMatkul.setText("Ringkasan Mata Kuliah");
        ringkasanMatkul.setFont(SistemAkademikGUI.fontTitle);
        ringkasanMatkul.setAlignmentX(Component.CENTER_ALIGNMENT);
        ringkasanMatkul.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e ){
                panel.setVisible(false); // Set visible false
                frame.remove(panel); // remove panel
                new RingkasanMataKuliahGUI(frame, daftarMahasiswa, daftarMataKuliah); // Menuju halaman ringkasan matkul
            }
        });
        
        // Add label dan button ke dalam panel
        panel.add(Box.createVerticalStrut(30));
        panel.add(titleLabel);
        panel.add(Box.createVerticalStrut(50));
        panel.add(tambahMahasiswa);
        panel.add(Box.createVerticalStrut(20));
        panel.add(tambahMatkul);
        panel.add(Box.createVerticalStrut(20));
        panel.add(tambahIRS);
        panel.add(Box.createVerticalStrut(20));
        panel.add(hapusIRS);
        panel.add(Box.createVerticalStrut(20));
        panel.add(ringkasanMhs);
        panel.add(Box.createVerticalStrut(20));
        panel.add(ringkasanMatkul);
        
        // Add panel ke dalam frame
        frame.add(panel);
        
    }
}
