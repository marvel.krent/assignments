package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.JOptionPane;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMahasiswaGUI{

    public TambahMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
       
        // Membuat panel mahasiswa
        JPanel panel = new JPanel();
        panel.setLayout(null);
        
        // Membuat label judul
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah Mahasiswa");
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setBounds(150,10,200,100);

        // Membuat label nama mahasiswa
        JLabel nameLabel = new JLabel();
        nameLabel.setText("Nama: ");
        nameLabel.setFont(SistemAkademikGUI.fontGeneral);
        nameLabel.setBounds(220,60,200,100);
        
        // Membuat field nama mahasiswa
        JTextField fieldNama = new JTextField();
        fieldNama.setFont(SistemAkademikGUI.fontGeneral);
        fieldNama.setBounds(150,130,200,30);
        
        // Membuat label NPM mahasiswa
        JLabel npmLabel = new JLabel();
        npmLabel.setText("NPM: ");
        npmLabel.setFont(SistemAkademikGUI.fontGeneral);
        npmLabel.setBounds(225, 165, 200, 30);
        
        // Membuat field NPM mahasiswa
        JTextField fieldNPM = new JTextField();
        fieldNPM.setFont(SistemAkademikGUI.fontGeneral);
        fieldNPM.setBounds(150,200,200,30);
        
        // Membuat button tambah
        JButton tambahButton = new JButton();
        tambahButton.setText("Tambahkan");
        tambahButton.setFont(SistemAkademikGUI.fontGeneral);
        tambahButton.setBounds(165,250,170,30);
        tambahButton.setBackground(Color.decode("#03C03C"));
        tambahButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                try{
                    // Set variabel masing2 dari field
                    String nama = fieldNama.getText();
                    Long npm = Long.parseLong(fieldNPM.getText());
                    Mahasiswa mhs = new Mahasiswa(nama, npm); // membuat objek mahasiswa

                    // Pengecekan field nama
                    if (nama.isBlank()){
                        JOptionPane.showMessageDialog(null, "Mohon isi seluruh Field");
                    } else {
                        // Pengecekan apakah sudah ada mahasiswa dengan npm sama
                        boolean check = true;
                        for (int i = 0; i < daftarMahasiswa.size(); i++){
                            if (daftarMahasiswa.get(i).getNpm() == npm){
                                check = false;
                                break;
                            }
                        }   
                        // Apabila blm ada mahasiswa dengan npm sama sebelumnya
                        if (check){
                            daftarMahasiswa.add(mhs); // Menambahkan mahasiswa ke dalam daftar mahasiswa
                            // Menampilkan kotak dialog
                            JOptionPane.showMessageDialog(null, String.format("Mahasiswa %d-%s berhasil ditambahkan", npm, nama));
                            fieldNama.setText(null); // Mengosongkan field nama
                            fieldNPM.setText(null); // Mengosongkan field npm
                        } else {
                            // Menampilkan kotak dialog
                            JOptionPane.showMessageDialog(null, String.format("NPM %d sudah pernah ditambahkan sebelumnya", npm));
                            fieldNama.setText(null); // Mengosongkan field nama
                            fieldNPM.setText(null); // Mengosongkan field npm
                        }
                    }

            
                }catch(NumberFormatException nfe){ // Exception fieldtext NPM kosong
                    // Menampilkan kotak dialog
                    JOptionPane.showMessageDialog(null, "Mohon isi seluruh Field");
                }
            }
        });
        
        // Membuat button kembali
        JButton kembaliButton = new JButton();
        kembaliButton.setText("Kembali");
        kembaliButton.setFont(SistemAkademikGUI.fontGeneral
        );
        kembaliButton.setBounds(175,300,150,30);
        kembaliButton.setBackground(Color.decode("#7CB9E8"));
        kembaliButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                panel.setVisible(false); // Set visibility panel menjadi false
                frame.remove(panel); // remove panel
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah); // Kembali ke halaman home
            }
        });

        // Menambahkan label, button, dan field yang ada ke dalam panel
        panel.add(titleLabel);
        panel.add(nameLabel);
        panel.add(fieldNama);
        panel.add(npmLabel);
        panel.add(fieldNPM);
        panel.add(tambahButton);
        panel.add(kembaliButton);

        // Menambahkan panel ke dalam frame
        frame.add(panel);
    }
}
