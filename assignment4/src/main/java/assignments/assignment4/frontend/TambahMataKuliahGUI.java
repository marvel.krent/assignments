package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class TambahMataKuliahGUI{

    public TambahMataKuliahGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){
        
        // Membuat panel halaman mata kuliah
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        // Membuat label judul 
        JLabel titleLabel = new JLabel();
        titleLabel.setText("Tambah Mata Kuliah");
        titleLabel.setFont(SistemAkademikGUI.fontTitle);
        titleLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Membuat label kode matkul
        JLabel kodeLabel = new JLabel();
        kodeLabel.setText("Kode Mata Kuliah:");
        kodeLabel.setFont(SistemAkademikGUI.fontGeneral);
        kodeLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Membuat field kode matkul
        JTextField fieldKode = new JTextField(10);
        fieldKode.setMaximumSize(new Dimension(200, 30));

        // Membuat label nama matkul
        JLabel namaLabel = new JLabel();
        namaLabel.setText("Nama Mata Kuliah:");
        namaLabel.setFont(SistemAkademikGUI.fontGeneral);
        namaLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Membuat field nama matkul
        JTextField fieldNama = new JTextField();
        fieldNama.setMaximumSize(new Dimension(200, 30));
        
        // Membuat label sks matkul
        JLabel sksLabel = new JLabel();
        sksLabel.setText("SKS:");
        sksLabel.setFont(SistemAkademikGUI.fontGeneral);
        sksLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        // Membuat field sks matkul
        JTextField fieldSks = new JTextField();
        fieldSks.setMaximumSize(new Dimension(200, 30));
        
        // Membuat label kapasitas
        JLabel kapasitasLabel = new JLabel();
        kapasitasLabel.setText("Kapasitas:");
        kapasitasLabel.setFont(SistemAkademikGUI.fontGeneral);
        kapasitasLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        
        // Membuat field kapasitas matkul
        JTextField fieldKapasitas = new JTextField();
        fieldKapasitas.setMaximumSize(new Dimension(200,30));
        
        // Membuat button tambah
        JButton tambahButton = new JButton();
        tambahButton.setText("Tambahkan");
        tambahButton.setFont(SistemAkademikGUI.fontGeneral);
        tambahButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        tambahButton.setBackground(Color.decode("#03C03C"));
        tambahButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                try{
                    // Set variabel masing2 field
                    String kode = fieldKode.getText();
                    String nama = fieldNama.getText();
                    int kapasitas = Integer.parseInt(fieldKapasitas.getText());
                    int sks = Integer.parseInt(fieldSks.getText());
                    MataKuliah matkul = new MataKuliah(kode, nama, sks, kapasitas);

                    // Pengecekan apakah matkul sudah ditambahkan sebelumnya
                    boolean check = true;
                    for (int i = 0; i < daftarMataKuliah.size(); i++){
                        if (daftarMataKuliah.get(i).getNama().equals(nama)){
                            check = false;
                            break;
                        }
                    }
                    // Pengecekan field nama dan kode 
                    if (nama.isBlank() || kode.isBlank()){
                        JOptionPane.showMessageDialog(null, "Mohon isi seluruh Field");
                    } else {
                        // Apabila blm ada matkul dengan nama sama sebelumnya
                        if (check){
                            daftarMataKuliah.add(matkul); // Menambahkan matkul ke dalam daftar matkul
                            // Menampilkan kotak dialog
                            JOptionPane.showMessageDialog(null, String.format("Mata Kuliah %s berhasil ditambahkan", nama));
                            fieldNama.setText(null); // Mengosongkan field nama
                            fieldKode.setText(null); // Mengosongkan field kode
                            fieldKapasitas.setText(null); // Mengosongkan field kapasitas
                            fieldSks.setText(null); // Mengosongkan field sks
                        } else {
                            // Menampilkan kotak dialog
                            JOptionPane.showMessageDialog(null, String.format("Mata Kuliah %s sudah pernah ditambahkan sebelumnya", nama));
                            fieldNama.setText(null); // Mengosongkan field nama
                            fieldKode.setText(null); // Mengosongkan field kode
                            fieldKapasitas.setText(null); // Mengosongkan field kapasitas
                            fieldSks.setText(null); // Mengosongkan field sks
                        }
                    }
                
                } catch (NumberFormatException nfe){ // Exception ada fieldtext kosong
                    // Menampilkan kotak dialog
                    JOptionPane.showMessageDialog(null, "Mohon isi seluruh Field");
                }
            }
        });

        // Membuat button kembali
        JButton kembaliButton = new JButton();
        kembaliButton.setText("Kembali");
        kembaliButton.setFont(SistemAkademikGUI.fontGeneral);
        kembaliButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembaliButton.setBackground(Color.decode("#7CB9E8"));
        kembaliButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                panel.setVisible(false); // Set visibility panel jadi false
                frame.remove(panel); // remove panel
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah); // Kembali ke halaman home
            }
        });

        // Menambahkan button, field, dan label ke dalam panel
        panel.add(Box.createVerticalStrut(40));
        panel.add(titleLabel);
        panel.add(Box.createVerticalStrut(25));
        panel.add(kodeLabel);
        panel.add(Box.createVerticalStrut(10));
        panel.add(fieldKode);
        panel.add(Box.createVerticalStrut(15));
        panel.add(namaLabel);
        panel.add(Box.createVerticalStrut(10));
        panel.add(fieldNama);
        panel.add(Box.createVerticalStrut(15));
        panel.add(sksLabel);
        panel.add(Box.createVerticalStrut(10));
        panel.add(fieldSks);
        panel.add(Box.createVerticalStrut(15));
        panel.add(kapasitasLabel);
        panel.add(Box.createVerticalStrut(10));
        panel.add(fieldKapasitas);
        panel.add(Box.createVerticalStrut(15));
        panel.add(tambahButton);
        panel.add(Box.createVerticalStrut(15));
        panel.add(kembaliButton);

        // Menambahkan panel ke dalam frame
        frame.add(panel);
    }
    
}
