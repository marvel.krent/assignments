package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Arrays;

import assignments.assignment4.backend.*;

public class DetailRingkasanMataKuliahGUI {
    public DetailRingkasanMataKuliahGUI(JFrame frame, MataKuliah mataKuliah, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        // Membuat panel halaman ringkasan matkul
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        // Membuat label judul halaman
        JLabel  judulLabel = new JLabel();
        judulLabel.setText("Detail Ringkasan Mata Kuliah");
        judulLabel.setFont(SistemAkademikGUI.fontTitle);
        judulLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(Box.createVerticalStrut(50)); // Sebagai jeda (jarak)
        panel.add(judulLabel); // Menambahkan label judul ke dalam panel
        
        // Membuat label nama
        JLabel namaLabel = new JLabel();
        namaLabel.setText(String.format("Nama mata kuliah: %s", mataKuliah.getNama()));
        namaLabel.setFont(SistemAkademikGUI.fontGeneral);
        namaLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(Box.createVerticalStrut(20));
        panel.add(namaLabel); // Menambahkan label nama ke dalam panel
        
        // Membuat label kode matkul
        JLabel kodeLabel = new JLabel();
        kodeLabel.setText(String.format("Kode: %s", mataKuliah.getKode()));
        kodeLabel.setFont(SistemAkademikGUI.fontGeneral);
        kodeLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(Box.createVerticalStrut(20));
        panel.add(kodeLabel); // Menambahkan label kode ke dalam panel
        
        // Membuat label sks matkul
        JLabel sksLabel = new JLabel();
        sksLabel.setText(String.format("SKS: %d", mataKuliah.getSKS()));
        sksLabel.setFont(SistemAkademikGUI.fontGeneral);
        sksLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(Box.createVerticalStrut(20));
        panel.add(sksLabel); // Menambahkan label sks ke dalam panel
        
        // Membuat label jumlah mahasiswa
        JLabel jumlahLabel = new JLabel();
        jumlahLabel.setText(String.format("Jumlah mahasiswa: %d", mataKuliah.getJumlahMahasiswa()));
        jumlahLabel.setFont(SistemAkademikGUI.fontGeneral);
        jumlahLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(Box.createVerticalStrut(20));
        panel.add(jumlahLabel); // Menambahkan label jumlah mahasiswa ke dalam panel
        
        // Membuat label kapasitas matkul
        JLabel kapasitasLabel = new JLabel();
        kapasitasLabel.setText(String.format("Kapasitas: %d", mataKuliah.getKapasitas()));
        kapasitasLabel.setFont(SistemAkademikGUI.fontGeneral);
        kapasitasLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(Box.createVerticalStrut(20));
        panel.add(kapasitasLabel); // Menambahkan label kapasitas ke dalam panel
        
        // Membuat label daftar
        JLabel daftarLabel = new JLabel();
        daftarLabel.setText("Daftar Mahasiswa:");
        daftarLabel.setFont(SistemAkademikGUI.fontGeneral);
        daftarLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(Box.createVerticalStrut(20));
        panel.add(daftarLabel); // Menambahkan label daftar ke dalam panel
        
        // Conditional apabila jumlah mahasiswa nol
        if (mataKuliah.getJumlahMahasiswa() <= 0){
            // Membuat label mahasiswa
            JLabel mhsKosongLabel = new JLabel();
            mhsKosongLabel.setText("Belum ada mahasiswa yang mengambil mata kuliah ini.");
            mhsKosongLabel.setFont(DetailRingkasanMahasiswaGUI.fontRingkasan); //set font
            mhsKosongLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
            panel.add(Box.createVerticalStrut(20));
            panel.add(mhsKosongLabel); // Menambahkan label mahasiswa ke dalam panel
        } else{
            // Iterasi untuk setiap mahasiswa yang terdapat dalam matkul
            for(int i = 0; i < mataKuliah.getDaftarMahasiswa().length; i++){
                if (mataKuliah.getDaftarMahasiswa()[i] != null){
                    // Membuat label mahasiswa
                    JLabel mhsLabel = new JLabel();
                    mhsLabel.setText(String.format("%d. %s", i+1, mataKuliah.getDaftarMahasiswa()[i].getNama()));
                    System.out.println(Arrays.toString(mataKuliah.getDaftarMahasiswa()));
                    mhsLabel.setFont(DetailRingkasanMahasiswaGUI.fontRingkasan);
                    mhsLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
                    panel.add(Box.createVerticalStrut(20));
                    panel.add(mhsLabel); // Menambahkan label mahasiswa ke dalam panel
                }
            }
        }
        
        // Membuat button kembali
        JButton kembaliButton = new JButton();
        kembaliButton.setText("Selesai");
        kembaliButton.setFont(SistemAkademikGUI.fontGeneral);
        kembaliButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembaliButton.setBackground(Color.decode("#03C03C"));
        kembaliButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                panel.setVisible(false); // Set visibility panel menjadi false
                frame.remove(panel); // remove panel
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah); // Kembali ke halaman home
            }
        });
        panel.add(Box.createVerticalStrut(20));
        panel.add(kembaliButton); // Menambahkan button kembali ke dalam panel

        // Menambahkan panel ke dalam frame
        frame.add(panel);
        
    }
}
