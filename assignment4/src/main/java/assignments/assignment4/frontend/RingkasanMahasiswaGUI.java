package assignments.assignment4.frontend;

import java.awt.*;
import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;

import assignments.assignment4.backend.*;

public class RingkasanMahasiswaGUI {

    // Atribut class
    private ArrayList<Mahasiswa> daftarMahasiswa;

    public RingkasanMahasiswaGUI(JFrame frame, ArrayList<Mahasiswa> daftarMahasiswa, ArrayList<MataKuliah> daftarMataKuliah){

        this.daftarMahasiswa = daftarMahasiswa;

        // Membuat panel halaman ringkasan
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        // Membuat judul halaman
        JLabel sambutan = new JLabel();
        sambutan.setText("Ringkasan Mahasiswa");
        sambutan.setFont(SistemAkademikGUI.fontTitle);
        sambutan.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Membuat label pilih npm
        JLabel pilihNPMLabel = new JLabel();
        pilihNPMLabel.setText("Pilih NPM");
        pilihNPMLabel.setFont(SistemAkademikGUI.fontGeneral);
        pilihNPMLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        // Membuat list baru untuk npm tiap mahasiswa
        ArrayList<Long> listNPM = new ArrayList<Long>();
        for (int i = 0; i < daftarMahasiswa.size(); i++){
            listNPM.add(daftarMahasiswa.get(i).getNpm());
        }
        // Mengubah list menjadi array
        Long[] arrayNPM = new Long[listNPM.size()];
        arrayNPM = listNPM.toArray(arrayNPM);

        // Sorting array
        for (int i  = 0; i < arrayNPM.length-1; i ++){
            for (int j = i + 1; j < arrayNPM.length; j ++){
                if (arrayNPM[i] > arrayNPM[j]){
                    long temp = arrayNPM[j];
                    arrayNPM[j] = arrayNPM[i];
                    arrayNPM[i] = temp;
                }
            }
        }
        // Membuat Dropdown pilih NPM
        JComboBox<Long> pilihNPMComboBox = new JComboBox<>(arrayNPM);
        pilihNPMComboBox.setMaximumSize(new Dimension(200,30));

        // Membuat button lihat
        JButton lihatButton = new JButton();
        lihatButton.setText("Lihat");
        lihatButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        lihatButton.setFont(SistemAkademikGUI.fontGeneral);
        lihatButton.setBackground(Color.decode("#03C03C"));
        lihatButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                try{
                    // Set variabel
                    long npm = (long) pilihNPMComboBox.getSelectedItem();
                    // Memanggil method cekIRS untuk mengecek masalah
                    getMahasiswa(npm).cekIRS();
                    panel.setVisible(false); // set visibility false
                    frame.remove(panel); // remove panel
                    new DetailRingkasanMahasiswaGUI(frame, getMahasiswa(npm), daftarMahasiswa, daftarMataKuliah); // Ganti ke halaman detail ringkasan mahasiswa
                } catch (NullPointerException npe){ // apabila terdapat field yang kosong
                    JOptionPane.showMessageDialog(null, "Mohon isi seluruh Field");
                }
            }
        });
        
        // Membuat button kembali
        JButton kembaliButton = new JButton();
        kembaliButton.setText("Kembali");
        kembaliButton.setFont(SistemAkademikGUI.fontGeneral);
        kembaliButton.setAlignmentX(Component.CENTER_ALIGNMENT);
        kembaliButton.setBackground(Color.decode("#7CB9E8"));
        kembaliButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                panel.setVisible(false); // Set visibility panel menjadi false
                frame.remove(panel); // remove panel
                new HomeGUI(frame, daftarMahasiswa, daftarMataKuliah); // Kembali ke halaman home
            }
        });

        // Menambahkan button, field, dan label ke dalam panel
        panel.add(Box.createVerticalStrut(50)); // Sebagai jeda
        panel.add(sambutan);
        panel.add(Box.createVerticalStrut(20));
        panel.add(pilihNPMLabel);
        panel.add(Box.createVerticalStrut(20));
        panel.add(pilihNPMComboBox);
        panel.add(Box.createVerticalStrut(20));
        panel.add(lihatButton);
        panel.add(Box.createVerticalStrut(20));
        panel.add(kembaliButton);

        // Menambahkan panel ke dalam frame
        frame.add(panel);
    }

    // Method getter
    private Mahasiswa getMahasiswa(long npm) {

        for (Mahasiswa mahasiswa : daftarMahasiswa) {
            if (mahasiswa.getNpm() == npm){
                return mahasiswa;
            }
        }
        return null;
    }
    
}
