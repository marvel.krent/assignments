package assignments.assignment3;

import java.util.Arrays;

class Mahasiswa extends ElemenFasilkom {
    
    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private MataKuliah[] daftarMataKuliah = new MataKuliah[10];
    private long npm;
    private String tanggalLahir;
    private String jurusan;
    private boolean cekMasalah;
    private boolean cekMasalahDrop;

    // Constructor
    Mahasiswa(String nama, long npm) {
        /* TODO: implementasikan kode Anda di sini */
        this.nama = nama;
        this.npm = npm;
    }

    // Method getter 
    public long getNpmMahasiswa(){
        return this.npm;
    }
    public MataKuliah[] getMataKuliah(){
        return this.daftarMataKuliah;
    }
    public boolean getCekMasalah(){
        return this.cekMasalah;
    }
    public boolean getCekMasalahDrop(){
        return this.cekMasalahDrop;
    }

    // Method addMatkul
    public void addMatkul(MataKuliah mataKuliah) {
        // Iterasi setiap daftar mata kuliah mahasiswa
        for (int i = 0; i < daftarMataKuliah.length; i++){
            // Apabila matkul telah diambil sebelumnya
            if (Arrays.asList(daftarMataKuliah).contains(mataKuliah)){
                System.out.printf("[DITOLAK] %s telah diambil sebelumnya\n",mataKuliah);
                this.cekMasalah = false; // Set masalah menjadi false
                break;
            } else if (mataKuliah.getKapasitas() == mataKuliah.getjmlMahasiswa()){ // Apabila kapasitas matkul penuh
                System.out.printf("[DITOLAK] %s telah penuh kapasitasnya\n", mataKuliah);
                this.cekMasalah = false; // Set masalah menjadi false
                break;
            } else if (daftarMataKuliah[i] == null){ // Menambahkan matkul pada array
                daftarMataKuliah[i] = mataKuliah;
                this.cekMasalah = true; // Set masalah menjadi true
                break;
            }
        }
    }

    // Method dropMatkul
    public void dropMatkul(MataKuliah mataKuliah) {
        // Validasi apakah mata kuliah terdapat pada daftar mata kuliah
        if (Arrays.asList(daftarMataKuliah).contains(mataKuliah)){
            for (int i = 0; i < daftarMataKuliah.length; i++){
                if (daftarMataKuliah[i] == null){
                    continue;
                }else if (daftarMataKuliah[i].equals(mataKuliah)){
                    daftarMataKuliah[i] = null; // Menghapus matakuliah pada daftar mata kuliah
                    break;
                }
            }
            System.out.printf("%s berhasil drop mata kuliah %s\n", this.nama, mataKuliah); 
            this.cekMasalahDrop = true; // Set masalah menjadi true
        } else {
            System.out.printf("[DITOLAK] %s belum pernah diambil\n", mataKuliah);
            this.cekMasalahDrop = false;
        }
    }

    public String extractTanggalLahir(long npm) {
        // membuat long NPM menjadi array
        String[] arrNPM = Long.toString(npm).split("");
        String tanggal = String.valueOf(getAngka(arrNPM, 4, 6));
        //memanggil method get angka untuk bulan lahir pada index 6 -- 8 yang lalu diassign ke variable bulan
        String bulan = String.valueOf(getAngka(arrNPM, 6, 8));
        //memanggil method get angka untuk tahun lahir pada index 8 -- 12 yang lalu diassign ke variable tahun
        String tahun = String.valueOf(getAngka(arrNPM, 8, 12));
        this.tanggalLahir = String.format("%s-%s-%s", tanggal,bulan,tahun);
        return this.tanggalLahir;
    }

    public String extractJurusan(long npm) {
        // Slicing pada bagian jurusan
        String[] extractjurusan = getSlice(Long.toString(npm).split(""), 2, 4);
        String[] ilmuKomputer = {"0","1"}; // Set variable untuk ilmu komputer
        // Conditional apabila ilmu komputer
        if (Arrays.equals(extractjurusan, ilmuKomputer)){
            this.jurusan = "Ilmu Komputer";
            return this.jurusan;
        }
        // Karena sudah dipastikan hanya IK dan SI maka dipastikan selain IK adalah SI
        this.jurusan = "Sistem Informasi";
        return this.jurusan;
    }

    //Membuat method slicing
    public static String[] getSlice(String[] arr, int startIndex,int endIndex){
        //Membuat copy dari array yang asli dengan range panjang sesuai yang mau dipotong
        return Arrays.copyOfRange(arr, startIndex,endIndex);
    }

    //Method get angka
    public static int getAngka(String[] arr,int indexStart,int indexEnd){
        //Slicing array di bagian umur
        String[] arrofAngka = getSlice(arr, indexStart, indexEnd);
        
        //Menggabungkan angka yang ingin dicari
        int angka = 0; //set variable untuk angka yang dicari
        int tipe = 1;  //set tipe bilangan (satuan, puluhan, ratusan, dst)
        for (int i = arrofAngka.length-1; i>=0; i--){
            int arrAngka = Integer.parseInt(arrofAngka[i]);
            angka += arrAngka*tipe;
            tipe*=10;
        } return angka;
    }
}