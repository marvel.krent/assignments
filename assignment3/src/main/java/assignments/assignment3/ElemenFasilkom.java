package assignments.assignment3;

import java.util.Arrays;

abstract class ElemenFasilkom {
    
    // Atribut class ElemenFasilkom
    protected String tipe;
    protected String nama;
    protected int friendship;
    protected ElemenFasilkom[] telahMenyapa = new ElemenFasilkom[100];

    // Method menyapa
    public void menyapa(ElemenFasilkom elemenFasilkom) {
        // Validasi elemenFasilkom tersebut sudah pernah disapa sebelumnya
        if (Arrays.asList(telahMenyapa).contains(elemenFasilkom))
            System.out.printf("[DITOLAK] %s telah menyapa %s hari ini\n", this.nama, elemenFasilkom);
        else {
            // Memasukkan orang yang disapa ke dalam array telahMenyapa
            for (int i = 0; i < telahMenyapa.length; i++){
                if (telahMenyapa[i] == null){
                    telahMenyapa[i] = elemenFasilkom;
                    for (int j = 0; j < elemenFasilkom.telahMenyapa.length; j++){
                        if (elemenFasilkom.telahMenyapa[j] == null){
                            elemenFasilkom.telahMenyapa[j] = this;
                            break;
                        }
                    }
                    break;
                }
            }
            System.out.printf("%s menyapa dengan %s\n", this.nama, elemenFasilkom); // Mencetak sesuai format
        } 
    }

    // Method resetMenyapa
    public void resetMenyapa() {
        // Mengosongkan array telahMenyapa
        telahMenyapa = new ElemenFasilkom[telahMenyapa.length];
    }

    // Method membeliMakanan
    public void membeliMakanan(ElemenFasilkom pembeli, ElemenKantin penjual, String namaMakanan) {
        boolean validate = false; // Variable validasi 

        for (int i = 0; i < penjual.getDaftarMakanan().length; i++){
            // Mengecek apakah makanan dijual oleh penjual
            if (penjual.getDaftarMakanan()[i] == null){
                continue;
            } else if (penjual.getDaftarMakanan()[i].toString().equals(namaMakanan)){
                // Mencetak sesuai format
                System.out.printf("%s berhasil membeli %s seharga %d\n", pembeli, namaMakanan, penjual.getDaftarMakanan()[i].getHarga());
                validate = true; // Set validate menjadi true
                // MEnjumlahkan nilai friendship penjual dan pembeli sebanyak 1
                pembeli.friendship += 1; 
                penjual.friendship += 1;
                break;
            }
        }
        // Conditional apabila penjual tidak menjual makanan
        if (!validate)
            System.out.printf("[DITOLAK] %s tidak menjual %s\n", penjual, namaMakanan);
    }

    public String toString() {
        // Mengembalikan nama dari elemen fasilkom
        return this.nama;
    }
}