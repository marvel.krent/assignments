package assignments.assignment3;

class ElemenKantin extends ElemenFasilkom {
    
    // Atribut elemen kantin daftarMakanan
    private Makanan[] daftarMakanan = new Makanan[10];

    // Constructor ElemenKantin
    ElemenKantin(String nama) {
        this.nama = nama;
    }

    // Method setMakanan
    public void setMakanan(String nama, long harga) {
        // Iterasi daftar makanan
        for (int i = 0; i< daftarMakanan.length; i++){
            if (daftarMakanan[i]== null){
                daftarMakanan[i] = new Makanan(nama,harga); // Memasukkan objek makanan ke dalam daftar makanan
                System.out.printf("%s telah mendaftarkan makanan %s dengan harga %d\n", this.nama, nama, harga); // Mencetak sesuai format
                break;
            }else if (daftarMakanan[i].toString().equals(nama)) { // Conditional apabila sudah terdapat makanan di dalam daftar
                System.out.printf("[DITOLAK] %s sudah pernah terdaftar\n", nama); // Mencetak sesuai format
                break;
            }
        }
    }

    // Method getter
    public Makanan[] getDaftarMakanan(){
        return this.daftarMakanan;
    }

}