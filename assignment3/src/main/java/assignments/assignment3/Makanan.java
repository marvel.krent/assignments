package assignments.assignment3;

class Makanan {

    // Atribut Class Makanan
    private String nama;
    private long harga;

    // Constructor 
    Makanan(String nama, long harga) {
        this.nama = nama;
        this.harga = harga;
    }

    //Method getter
    public long getHarga(){
        return this.harga;
    }

    // Mengembalikan nama dari Makanan
    public String toString() {
        return this.nama;
    }
}