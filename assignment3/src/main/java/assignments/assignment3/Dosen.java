package assignments.assignment3;

class Dosen extends ElemenFasilkom {

    // Atribut Class Dosen
    private MataKuliah mataKuliah;
    private boolean cekMasalahDosen;

    // Constructor
    Dosen(String nama) {
        this.nama = nama;
    }

    // Method Getter
    public MataKuliah getMatkul(){
        return this.mataKuliah;
    }
    public boolean getCekMasalahDosen(){
        return this.cekMasalahDosen;
    }

    // Method mengajarMataKuliah
    public void mengajarMataKuliah(MataKuliah mataKuliah) {
        // Conditional apabila dosen belum mengajar matkul apapun dan matkul belum ada dosen
        if (this.mataKuliah == null && mataKuliah.getDosen() == null) {
            this.mataKuliah = mataKuliah;
            // Mencetak sesuai format
            System.out.printf("%s mengajar mata kuliah %s\n", this.nama, mataKuliah);
            this.cekMasalahDosen = true;
        }else if (this.mataKuliah != null){ // Conditional apabila sudah ada dosen yang mengajar pada matkul
            // Mencetak sesuai format
            System.out.printf("[DITOLAK] %s sudah mengajar mata kuliah %s\n", this.nama, this.mataKuliah);
            this.cekMasalahDosen = false;
        }else if (mataKuliah.getDosen() != null){ // Conditional apabila dosen sudah mengajar di mata kuliah tertentu
            // Mencetak sesuai format
            System.out.printf("[DITOLAK] %s sudah memiliki dosen pengajar\n", mataKuliah);
            this.cekMasalahDosen = false;
        }
    }

    // Method dropMataKuliah
    public void dropMataKuliah() {
        // Conditional apabila dosen sedang tidak mengajar mata kuliah apapun
        if (this.mataKuliah == null){
            // Mencetak sesuai format
            System.out.printf("[DITOLAK] %s sedang tidak mengajar mata kuliah apapun\n", this.nama);
        } else {
            // Memanggil method dropDosen pada matkul
            mataKuliah.dropDosen();
            // Mencetak sesuai format
            System.out.printf("%s berhenti mengajar %s\n", this.nama, mataKuliah);
            // Menngubah this.mataKuliah menjadi null
            this.mataKuliah = null;
        }
    }
}