package assignments.assignment3;

class MataKuliah {

    // Atribut Class Mata kuliah
    private String nama;
    private int kapasitas;
    private Dosen dosen;
    private Mahasiswa[] daftarMahasiswa;
    private int jmlMahasiswa;

    // Constructor
    MataKuliah(String nama, int kapasitas) {
        this.nama = nama;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[this.kapasitas];
    }
    
    // Method getter
    public int getKapasitas() {
        return this.kapasitas;
    }
    public Mahasiswa[] getDaftarMahasiswa(){
        return this.daftarMahasiswa;
    }
    public Dosen getDosen(){
        return this.dosen;
    }
    public int getjmlMahasiswa(){
        return this.jmlMahasiswa;
    }

    // Method addMahasiswa
    public void addMahasiswa(Mahasiswa mahasiswa) {
        // Iterasi untuk setiap daftar mahasiswa
        for (int i = 0; i < daftarMahasiswa.length; i++){
            // Memasukkan mahasiswa ke dalam daftar mahasiswa
            if (daftarMahasiswa[i] == null){
                daftarMahasiswa[i] = mahasiswa;
                jmlMahasiswa++; //Menambahkan jumlah mahasiswa pada suatu matkul
                break;
            }
        }
    }

    // Method dropMahasiswa
    public void dropMahasiswa(Mahasiswa mahasiswa) {
        // Iterasi untuk setiap daftar mahasiswa
        for (int i = 0; i < daftarMahasiswa.length; i++){
            // Mengubah mahasiswa yang di drop menjadi null
            if (daftarMahasiswa[i] == mahasiswa){
                daftarMahasiswa[i] = null;
                jmlMahasiswa--; // Mengurangi jumlah mahasiswa pada suatu matkul
                break;
            }
        }
    }

    // Method addDosen
    public void addDosen(Dosen dosen) {
        // Set dosen
        this.dosen = dosen;
    }

    // Method dropDosen
    public void dropDosen() {
        // Mengubah dosen manjadi null
        this.dosen = null;
    }

    // Mengembalikan nama matkul
    public String toString() {
        return this.nama;
    }
}