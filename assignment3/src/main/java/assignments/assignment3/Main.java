package assignments.assignment3;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    /* TODO: Silahkan menambahkan visibility pada setiap method dan variabel apabila diperlukan */

    private static ElemenFasilkom[] daftarElemenFasilkom = new ElemenFasilkom[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    private static int totalMataKuliah = 0;
    private static int totalElemenFasilkom = 0;

    // Method getElemen 
    private static ElemenFasilkom getElemen(String nama){
        // Looping untuk setiap elemen fasilkom di dalam daftar elemen fasilkom
        for (int i = 0; i < daftarElemenFasilkom.length; i++){
            if (daftarElemenFasilkom[i] == null){
                continue;
            }else if (daftarElemenFasilkom[i].toString().equals(nama)){
                // Mengembalikan object elemen fasilkom
                return daftarElemenFasilkom[i];
            } 
        }
        return null;
    }

    // Method mendapatkan object Matkul berdasarkan namanya
    private static MataKuliah getMatkul(String nama){
        // Iterasi untuk setiap daftar matkul di setiap elemen fasilkom
        for (int i = 0; i < daftarElemenFasilkom.length; i++){
            if (daftarMataKuliah[i].toString().equals(nama)){
                // Mengembalikan object Matkul yang dicari
                return daftarMataKuliah[i];
            } 
        }
        return null;
    }

    // Method add Mahasiswa
    private static void addMahasiswa(String nama, long npm) {
        // Membuat objek mahasiswa
        Mahasiswa mhs = new Mahasiswa(nama, npm);
        mhs.tipe = "Mahasiswa"; // Set tipe mahasiswa menjadi Mahasiswa
        // Iterasi untuk setiap daftar elemen fasilkom
        for (int i = 0; i < daftarElemenFasilkom.length; i++){
            // Conditional apabila null
            if (daftarElemenFasilkom[i] == null){
                // Memasukkan mahasiswa ke dalam daftar
                daftarElemenFasilkom[i] = mhs;
                // Menambahkan jumlah total elemen fasilkom
                totalElemenFasilkom++;
                // Mencetak sesuai format
                System.out.printf("%s berhasil ditambahkan\n", mhs);
                break;
            }
        }
    }
    
    // Method add Dosen
    private static void addDosen(String nama) {
        // Membuat objek dosen
        Dosen dosen = new Dosen(nama);
        dosen.tipe = "Dosen"; // Set tipe dosen menjadi Dosen
        // Iterasi untuk setiap daftar elemen fasilkom
        for (int i = 0; i < daftarElemenFasilkom.length; i++){
            if (daftarElemenFasilkom[i] == null){
                daftarElemenFasilkom[i] = dosen; // Memasukkan dosen ke daftar elemen fasilkom
                totalElemenFasilkom++; // Menambahkan jumlah total elemen fasilkom
                // Mencetak sesuai format
                System.out.printf("%s berhasil ditambahkan\n", dosen);
                break;
            }
        }
    }
    
    // Method add Elemen Kantin
    private static void addElemenKantin(String nama) {
        // Membuat objek elemenKantin
        ElemenKantin ekantin = new ElemenKantin(nama);
        ekantin.tipe = "Elemen Kantin"; // Set tipe elemen kantin menjadi Elemen Kantin
        // Iterasi untuk setiap daftar elemen fasilkom
        for (int i = 0; i < daftarElemenFasilkom.length; i++){
            if (daftarElemenFasilkom[i] == null){
                daftarElemenFasilkom[i] = ekantin; // Memasukkan elemen kantin ke daftar elemen fasilkom
                totalElemenFasilkom++; // Menambahkan jumlah total elemen fasilkom
                // Mencetak sesuai format
                System.out.printf("%s berhasil ditambahkan\n", ekantin);
                break;
            }
        }

    }

    // Method menyapa
    private static void menyapa(String objek1, String objek2) {
        // Validasi dua objek yang berbeda
        if (objek1.equals(objek2)){
            System.out.println("[DITOLAK] Objek yang sama tidak bisa saling menyapa");
        } else {
            // Conditional apabila objek 1 dosen dan objek 2 mahasiswa
            if (getElemen(objek1) instanceof Dosen && getElemen(objek2) instanceof Mahasiswa){
                // Membuat objek dosen dan mahasiswa
                Dosen dosen = (Dosen) getElemen(objek1);
                Mahasiswa mhs = (Mahasiswa) getElemen(objek2);
                if (dosen.getMatkul() != null){
                    // Apabila dosen satu matkul dengan mahasiswa
                    if (Arrays.asList(mhs.getMataKuliah()).contains(dosen.getMatkul())){
                        // Menambahkan jumlah friendship sebanyak 2 kedua objek
                        dosen.friendship += 2;
                        mhs.friendship += 2;
                    }
                }
            // Conditional apabila objek 1 Mahasiswa dan objek 2 Dosen
            } else if (getElemen(objek1) instanceof Mahasiswa && getElemen(objek2) instanceof Dosen){
                Dosen dosen = (Dosen) getElemen(objek2);
                Mahasiswa mhs = (Mahasiswa) getElemen(objek1);
                if (dosen.getMatkul() != null){
                    // Apabila dosen satu matkul dengan mahasiswa
                    if (Arrays.asList(mhs.getMataKuliah()).contains(dosen.getMatkul())){
                        // Menambahkan jumlah friendship sebanyak 2 kedua objek
                        dosen.friendship += 2;
                        mhs.friendship += 2;
                    }
                }
            }
            // Memanggil method menyapa
            getElemen(objek1).menyapa(getElemen(objek2));
        }
    }

    // Method add Makanan
    private static void addMakanan(String objek, String namaMakanan, long harga) {
        // Validasi elemen kantin
        if (getElemen(objek) instanceof ElemenKantin){
            // Membuat objek elemen kantin
            ElemenKantin elemenKantin = (ElemenKantin) getElemen(objek);
            // Memanggil fungsi set makanan
            elemenKantin.setMakanan(namaMakanan, harga);
        }else {
            // Mencetak sesuai format
            System.out.printf("[DITOLAK] %s bukan merupakan elemen kantin\n", objek);
        } 
    }

    // Method membeli makanan
    private static void membeliMakanan(String objek1, String objek2, String namaMakanan) {
        // Validasi apabila objek 2 merupakan elemen kantin
        if (getElemen(objek2) instanceof ElemenKantin){
            // Validasi apabila merupakan objek yang sama
            if (getElemen(objek1).equals(getElemen(objek2))){
                System.out.println("[DITOLAK] Elemen kantin tidak bisa membeli makanan sendiri");
            }else{
                // Membuat objek elemen kantin
                ElemenKantin elemenKantin = (ElemenKantin) getElemen(objek2);
                // Memanggil method membeli makanan
                elemenKantin.membeliMakanan(getElemen(objek1), elemenKantin, namaMakanan);
            }
        } else {
            System.out.println("[DITOLAK] Hanya elemen kantin yang dapat menjual makanan");
        }
    }

    // Method membuat matkul
    private static void createMatkul(String nama, int kapasitas) {
        // Membuat objek mata kuliah
        MataKuliah matkul = new MataKuliah(nama, kapasitas);
        // Iterasi setiap daftar mata kuliah
        for (int i = 0; i < daftarMataKuliah.length; i++){
            if (daftarMataKuliah[i] == null){
                daftarMataKuliah[i] = matkul; // Memasukkan matkul ke dalam daftar mata kuliah
                totalMataKuliah++; // Menambahkan jumlah total mata kuliah
                // Mencetak sesuai format
                System.out.printf("%s berhasil ditambahkan dengan kapasitas %d\n", nama, kapasitas);
                break;
            }
        }
    }

    // Method add Matkul
    private static void addMatkul(String objek, String namaMataKuliah) {
        // Validasi objek harus mahasiswa
        if (getElemen(objek) instanceof Mahasiswa){
            // Membuat objek mahasiswa dan objek mata kuliah
            Mahasiswa mhs = (Mahasiswa) getElemen(objek);
            MataKuliah matkul = getMatkul(namaMataKuliah);
            // Memanggil method add matkul
            mhs.addMatkul(matkul);
            // Apabila tidak ada masalah saat menambahkan matkul
            if (mhs.getCekMasalah()){
                matkul.addMahasiswa(mhs); // Memanggil method add mahasiswa
                System.out.printf("%s berhasil menambahkan mata kuliah %s\n", objek, namaMataKuliah);
            }
        }else {
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat menambahkan matkul");
        }
    }

    // Method drop matkul
    private static void dropMatkul(String objek, String namaMataKuliah) {
        // Validasi objek mahasiswa
        if (getElemen(objek) instanceof Mahasiswa){
            // Membuat objek mahasiswa dan mata kuliah
            Mahasiswa mhs = (Mahasiswa) getElemen(objek);
            MataKuliah mataKuliah = getMatkul(namaMataKuliah);
            // Memanggil method drop matkul
            mhs.dropMatkul(mataKuliah);
            // Validasi masalah pada drop matkul
            if (mhs.getCekMasalahDrop()){
                mataKuliah.dropMahasiswa(mhs);
            }
        } else{
            System.out.println("[DITOLAK] Hanya mahasiswa yang dapat drop matkul");
        }
    }

    // Method mengajar matkul
    private static void mengajarMatkul(String objek, String namaMataKuliah) {
        // Validasi objek Dosen
        if (getElemen(objek) instanceof Dosen){
            // Membuat objek dosen dan mata kuliah
            Dosen dosen = (Dosen) getElemen(objek);
            MataKuliah mataKuliah = getMatkul(namaMataKuliah);
            // Memanggil method mengajar mata kuliah
            dosen.mengajarMataKuliah(mataKuliah);
            // Mengecek apakah terdapat masalah atau tidak 
            if (dosen.getCekMasalahDosen()){
                mataKuliah.addDosen(dosen);
            }
        }else{
            System.out.println("[DITOLAK] Hanya dosen yang dapat mengajar matkul");
        }
    }
    
    // Method berhenti mengajar
    public static void berhentiMengajar(String objek) {
        // Validasi objek merupakan dosen
        if (getElemen(objek) instanceof Dosen){
            // Membuat objek dosen 
            Dosen dosen = (Dosen) getElemen(objek);
            dosen.dropMataKuliah(); // Memanggil method drop mata kuliah
        }else{
            System.out.println("[DITOLAK] Hanya dosen yang dapat berhenti mengajar");
        }
    }

    // Method ringkasan mahasiswa
    private static void ringkasanMahasiswa(String objek) {
        // Validasi objek mahasiswa
        if (getElemen(objek) instanceof Mahasiswa){
            // Membuat objek mahasiswa
            Mahasiswa mhs = (Mahasiswa) getElemen(objek);
            // Mencetak sesuai format
            System.out.printf("Nama: %s \nTanggal lahir: %s \nJurusan: %s \nDaftar Mata Kuliah: \n", mhs, mhs.extractTanggalLahir(mhs.getNpmMahasiswa()), mhs.extractJurusan(mhs.getNpmMahasiswa()));
            // Set Variabel boolean allnull
            boolean allNull = true;
            // Looping untuk daftar mata kuliah mahasiswa
            for(int i = 0; i < mhs.getMataKuliah().length; i++){
                if (mhs.getMataKuliah()[i] != null){
                    allNull = false; // Set variabel allNull menjadi false
                    break;
                } 
            }
            // Apabila isi dari array kosong semua (null), maka belum ada mata kuliah yang diambil
            if (allNull){
                System.out.println("Belum ada mata kuliah yang diambil");
            } else {
                //Set variable nomor
                int no = 1;
                //Looping untuk setiap array daftarMatkul
                for(int j = 0; j < mhs.getMataKuliah().length; j++){
                    if (mhs.getMataKuliah()[j] == null){
                        continue;
                    } else {
                        // Mencetak sesuai format
                        System.out.println(no + ". " + mhs.getMataKuliah()[j]);
                        no++;
                    }
                }
            }
        }else {
            System.out.printf("[DITOLAK] %s bukan merupakan seorang mahasiswa\n",objek);
        }

    }

    private static void ringkasanMataKuliah(String namaMataKuliah) {
        MataKuliah matkul = getMatkul(namaMataKuliah);
        // Handle kasus jika tidak ada dosen pengajar
        String outDosen = null;
        if (matkul.getDosen() == null){
            outDosen = "Belum ada";
        } else {
            outDosen = matkul.getDosen().toString();
        }
        System.out.printf("Nama mata kuliah: %s\nJumlah mahasiswa: %d\nKapasitas: %d\nDosen pengajar: %s\nDaftar mahasiswa yang mengambil mata kuliah ini:\n", namaMataKuliah, matkul.getjmlMahasiswa(),matkul.getKapasitas(), outDosen, matkul.getDosen());
        
        boolean allNull = true; //Set variable allNull
        //Looping untuk array DaftarMahasiswa
        for(int i = 0; i < matkul.getDaftarMahasiswa().length; i++){
            //Apabila menemukan mahasiswa di dalam array
            if(matkul.getDaftarMahasiswa()[i] != null){
                allNull = false; //Set variable allNull menjadi false
                break;
            }
        }
        //Apabila isi dari array kosong semua (null), maka belum ada mahasiswa yang mengambil
        if(allNull){
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
        } else {
            //Set variable nomor
            int no = 1;
            //Looping untuk setiap array daftarMahasiswa
            for(int j = 0; j< matkul.getDaftarMahasiswa().length; j++){
                if (matkul.getDaftarMahasiswa()[j] == null){
                    continue;
                } else {
                    //Mencetak sesuai format
                    System.out.println(no + ". " + matkul.getDaftarMahasiswa()[j]);
                    no++;
                }
            }
        }

    }

    private static void nextDay() {
        /* TODO: implementasikan kode Anda di sini */
        for (int i = 0; i < daftarElemenFasilkom.length; i++){
            if (daftarElemenFasilkom[i] != null){
                int count = 0;
                // Iterasi setiap elemen yang disapa
                for (int j = 0; j < daftarElemenFasilkom[i].telahMenyapa.length; j++){
                    if (daftarElemenFasilkom[i].telahMenyapa[j] != null)
                        count ++;
                }
                if (count >= (float)(totalElemenFasilkom-1)/2 ){
                    daftarElemenFasilkom[i].friendship += 10;
                } else{
                    daftarElemenFasilkom[i].friendship -= 5;
                }
                if (daftarElemenFasilkom[i].friendship >= 100){
                    daftarElemenFasilkom[i].friendship = 100;
                } else if (daftarElemenFasilkom[i].friendship < 0){
                    daftarElemenFasilkom[i].friendship = 0;
                }
                daftarElemenFasilkom[i].resetMenyapa();
            }
        } 
        System.out.println("Hari telah berakhir dan nilai friendship telah diupdate");
        friendshipRanking();
    }

    private static void friendshipRanking() {
        ElemenFasilkom[] sort = Arrays.copyOf(daftarElemenFasilkom, daftarElemenFasilkom.length);
        // Iterasi untuk sorting berdasarkan friendship
        for (int i = 0; i < sort.length; i ++){
            if (sort[i] != null){
                for (int j = 1; j < sort.length - 1; j ++){
                    if (sort[j] != null){
                        // Conditional apabila elemen setelahnya memiliki friendship yang lebih kecil
                        if (sort[j-1].friendship < sort[j].friendship){
                            ElemenFasilkom temp = sort[j-1]; // Menyimpan sementara
                            // Menukar posisi
                            sort[j-1] = sort[j];
                            sort[j] = temp;
                        }
                    }
                }

            }
        }
        // Iterasi untuk sorting berdasarkan abjad
        for (int i = 0; i < sort.length; i ++){
            if (sort[i] != null){
                for (int j = 1; j < sort.length - 1; j ++){
                    if (sort[j] != null){
                        // Conditional apabila memiliki friendship yang sama
                        if (sort[j-1].friendship == sort[j].friendship){
                            // Menyimpan value hasil compareTo
                            int value = sort[j-1].toString().compareTo(sort[j].toString());
                            // Apabila abjadnya lebih kecil
                            if (value > 0){
                                ElemenFasilkom temp = sort[j-1]; // Menyimpan sementara
                                // Menukar posisi
                                sort[j-1] = sort[j];
                                sort[j] = temp;
                            }
                        }
                    }
                } 
            }
        }
        // Mencetak sesuai output yang diminta
        for (int no = 0; no < sort.length; no++){
            if (sort[no] != null)
                System.out.printf("%d. %s(%d)\n", no+1, sort[no], sort[no].friendship);
        }
    }

    // Method programEnd
    private static void programEnd() {
        // Mencetak sesuai format
        System.out.println("Program telah berakhir. Berikut nilai terakhir dari friendship pada Fasilkom :");
        // Memanggil method untuk mencetak urutan ranking friendship
        friendshipRanking();
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        while (true) {
            String in = input.nextLine();
            if (in.split(" ")[0].equals("ADD_MAHASISWA")) {
                addMahasiswa(in.split(" ")[1], Long.parseLong(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_DOSEN")) {
                addDosen(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("ADD_ELEMEN_KANTIN")) {
                addElemenKantin(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("MENYAPA")) {
                menyapa(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("ADD_MAKANAN")) {
                addMakanan(in.split(" ")[1], in.split(" ")[2], Long.parseLong(in.split(" ")[3]));
            } else if (in.split(" ")[0].equals("MEMBELI_MAKANAN")) {
                membeliMakanan(in.split(" ")[1], in.split(" ")[2], in.split(" ")[3]);
            } else if (in.split(" ")[0].equals("CREATE_MATKUL")) {
                createMatkul(in.split(" ")[1], Integer.parseInt(in.split(" ")[2]));
            } else if (in.split(" ")[0].equals("ADD_MATKUL")) {
                addMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("DROP_MATKUL")) {
                dropMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("MENGAJAR_MATKUL")) {
                mengajarMatkul(in.split(" ")[1], in.split(" ")[2]);
            } else if (in.split(" ")[0].equals("BERHENTI_MENGAJAR")) {
                berhentiMengajar(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MAHASISWA")) {
                ringkasanMahasiswa(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("RINGKASAN_MATKUL")) {
                ringkasanMataKuliah(in.split(" ")[1]);
            } else if (in.split(" ")[0].equals("NEXT_DAY")) {
                nextDay();
            } else if (in.split(" ")[0].equals("PROGRAM_END")) {
                programEnd();
                break;
            }
        }
        input.close();
    }
}