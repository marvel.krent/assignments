package assignments.assignment1;

import java.util.Arrays;
import java.util.Scanner;

public class ExtractNPM {
    
    //Method mengubah array string menjadi array int
    public static int[] getArrOfInt(String[]arr){
        //Membuat variable baru newArr dengan panjang yang sama dengan arr
        int[] newArr = new int[arr.length];
        
        //Mengiterasikan elemen di dalam arr, lalu mengubah menjadi int yang dimasukkan ke dalam newArr
        for(int i = 0; i < arr.length; i++){
            newArr[i] = Integer.valueOf(arr[i]);
        }
        return newArr;
    }

    //Membuat method slicing
    public static String[] getSlice(String[] arr, int startIndex,int endIndex){
        //Membuat copy dari array yang asli dengan range panjang sesuai yang mau dipotong
        String[] slice = Arrays.copyOfRange(arr, startIndex,endIndex);
        return slice;
    }
    
    //Method get angka
    public static int getAngka(String[] arr,int indexStart,int indexEnd){
        //Slicing array di bagian umur
        String[] arrofAngka = getSlice(arr, indexStart, indexEnd);
        
        //Menggabungkan angka yang ingin dicari
        int angka = 0; //set variable untuk angka yang dicari
        int tipe = 1;  //set tipe bilangan (satuan, puluhan, ratusan, dst)
        for (int i = arrofAngka.length-1; i>=0; i--){
            int arrAngka = Integer.valueOf(arrofAngka[i]);
            angka += arrAngka*tipe;
            tipe*=10;
        } return angka;
    }

    //Method get jurusan
    public static String getJurusan(String[] arr){
        //Slicing array menjadi bagian kode jurusan
        String[] arrofJurusan = getSlice(arr, 2, 4);

        //Set array untuk setiap kode jurusan
        String[] ilmuKomputer = {"0","1"};
        String[] sistemInformasi = {"0","2"};
        String[] teknologiInformasi = {"0","3"};
        String[] teknikTelekomunikasi = {"1","1"};
        
        //Conditional mengecek apakah kode yang didapatkan sama dengan tiap kode jurusan yang ada
        if (Arrays.equals(arrofJurusan,ilmuKomputer)){
            return "Ilmu Komputer";
        } else if (Arrays.equals(arrofJurusan,sistemInformasi)){
            return "Sistem Informasi";
        } else if (Arrays.equals(arrofJurusan,teknologiInformasi)){
            return "Teknologi Informasi";
        } else if (Arrays.equals(arrofJurusan,teknikTelekomunikasi)){
            return "Teknik Telekomunikasi";
        } else { //karena sudah dicek pada tahap validate maka else yang lain hanya ada satu kemungkinan yaitu teknik elektro
            return "Teknik Elektro"; 
        }
    }


    //Method validasi cek umur
    public static boolean cekUmur (String[] arr){
        //Validasi umur >= 15 tahun
        if ((2000+getAngka(arr, 0, 2)) - getAngka(arr, 8, 12) >= 15){ //index 8 -- 12 untuk mengambil tahun lahir
            return true;
        } else {
            return false;
        }
    }

    //Method cek kode NPM
    public static boolean cekKode (String[] arr){
        //mengubah string[] menjadi int[]
        int[] intNewArr = getArrOfInt(arr);
        
        //Looping untuk menghitung kode npm
        int sum = 0; //set variabel sum terlebih dahulu
        for (int i = 1; i < intNewArr.length; i++){
            sum += intNewArr[i-1]*intNewArr[(intNewArr.length-1)-i]; //menjumlahkan index awal dengan index digit 13, 2 dengan 12, dst
            //Conditional apabila index sudah mencapai tengah/sama
            if (i-1 == (intNewArr.length-1)-i){
                break;
            }else{
                continue;
            }
        }
        
        //Apabila hasil perkalian & penjumlahan (sum) sudah kurang dari 10
        if (sum < 10){
            //melakukan pengecekan dengan kode trakhir pada npm
            System.out.println("a"+sum);
            if (sum == intNewArr[intNewArr.length-1]){
                return true;
            }
            
            //Apabila hasil perkalian & penjumlahan (sum) lebih dari 10
        }else{ 
            System.out.println("b"+sum);
            //masuk ke method rekursif untuk menjumlahkan tiap digit
            int kodeNpm = rekursif(sum);
            //melakukan pengecekan dengan kode trakhir pada npm
            if (kodeNpm == intNewArr[intNewArr.length-1]){
                return true;
            }
        }
        //Apabila tidak sesuai kondisi return false
        return false;
    }

    //method rekursif untuk menghitung kembali sum
    public static int rekursif(int res){
        //Base case
        if (res < 10){
            return res;
        //Rekursif case
        }else {
            // return rekursif(getArrOfInt(Integer.toString(res).split(""))[0] + getArrOfInt(Integer.toString(res).split(""))[1]); //mengubah integer menjadi array of integer, yang lalu diambil indexnya dan dijumlahkan
            if (res>= 100){
                return rekursif((int)(res/100 + (res%100) /10 + res%10));
            }else{
                return rekursif((int)(res/10 + res%10));
            }
        }

    }

    //Method validate
    public static boolean validate(long npm) {       
        //Membuat tipe data NPM menjadi string, lalu diubah menjadi array
        String[] arrofNpm = Long.toString(npm).split("");
        
        //looping NPM untuk menghitung jumlah digitnya
        int itung = 0; //deklarasi var itung untuk menghitung
        for (String i : arrofNpm){
            itung++;
        }
        
        //Validasi jumlah digit pada NPM 
        if (itung != 14){
            return false;
        }else {
            //Validate kode jurusan
            if (arrofNpm[2].equals("0")){
                if (arrofNpm[3].equals("1") || (arrofNpm[3].equals("2") || arrofNpm[3].equals("3"))){
                    //Validate umur                     
                    if (cekUmur(arrofNpm)){
                        //Validate kode npm
                        if(cekKode(arrofNpm)){
                            return true;
                        }
                    } 
                }
            }else if (arrofNpm[2].equals("1")){
                if (arrofNpm[3].equals("1") || (arrofNpm[3].equals("2"))){  
                    //Validate umur
                    if (cekUmur(arrofNpm)){
                        //Validate kode npm
                        if (cekKode(arrofNpm)){
                            return true;   
                        }
                    }
                }
            }
        }
        //Apabila terdapat kondisi yang tidak memenuhi akan return false
        return false;
    }

    //Method extract
    public static String extract(long npm) {
        //Membuat long npm menjadi array of string
        String[] arrofNpm = Long.toString(npm).split("");
        //menggabungkan hasil ekstrak taun masuk dengan "20" supaya menjadi "20xx"
        String tahunMasuk = "20"+String.valueOf(getAngka(arrofNpm, 0, 2));
        //memanggil method get jurusan yang lalu diassign ke variable jurusan
        String jurusan = getJurusan(arrofNpm);
        //memanggil method get angka untuk tanggal lahir pada index 4 -- 6 yang lalu diassign ke variable tanggal
        String tanggal = String.valueOf(getAngka(arrofNpm, 4, 6));
        //memanggil method get angka untuk bulan lahir pada index 6 -- 8 yang lalu diassign ke variable bulan
        String bulan = String.valueOf(getAngka(arrofNpm, 6, 8));
        //memanggil method get angka untuk tahun lahir pada index 8 -- 12 yang lalu diassign ke variable tahun
        String tahun = String.valueOf(getAngka(arrofNpm, 8, 12));
        
        //Mengecek apabila tanggal hanya terdiri dari satu digit, maka akan ada penambahan nol didepannya
        if (tanggal.length() == 1){
            tanggal = "0"+tanggal;
        }
        
        //Mengecek apabila bulan hanya terdiri dari satu digit, maka akan ada penambahan nol didepannya
        if (bulan.length() == 1){
            bulan = "0"+bulan;
        }
        //mengembalikan sesuai dengan format yang diminta
        return "Tahun masuk: "+tahunMasuk+"\nJurusan: "+ jurusan+ "\nTanggal Lahir: "+tanggal+"-"+bulan+"-"+tahun;
    }
    
    //Main Program
    public static void main(String args[]) {
        Scanner input = new Scanner(System.in);
        boolean exitFlag = false;
        while (!exitFlag) {
            long npm = input.nextLong();
            if (npm < 0) {
                exitFlag = true;
                break;
            }

            //Memanggil method validate
            validate(npm);

            //conditional apabila method validate mengembalikan nilai true
            if (validate(npm)){
                //akan mencetak method extract yang mengembalikan sesuai format
                System.out.println(extract(npm));
            }else{ //apabila method mengembalikan nilai false
                //akan mencetak "NPM tidak valid!"
                System.out.println("NPM tidak valid!");
            }
            
        }
        input.close();
    }
}