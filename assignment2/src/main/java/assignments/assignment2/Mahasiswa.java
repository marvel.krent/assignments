package assignments.assignment2;

import java.util.Arrays;

public class Mahasiswa {
    private MataKuliah[] mataKuliah = new MataKuliah[10];
    private String[] masalahIRS;
    private int totalSKS;
    private String nama;
    private String jurusan;
    private long npm;
    private int numOfMatkul;
    private int numOfMasalah;

    //Constructor class Mahasiswa
    public Mahasiswa(String nama, long npm){
        this.nama = nama;
        this.npm = npm;
        this.masalahIRS = new String[20]; 
    }

    //Getter
    public String getNama() {
        return this.nama;
    }

    public long getNpm(){
        return this.npm;
    }

    public MataKuliah[] getMataKuliah(){
        return this.mataKuliah;
    }

    public int getNumOfMatkul(){
        return this.numOfMatkul;
    }

    public String getJurusan(){
        //Cek jurusan pada NPM
        String[] arrofNpm = Long.toString(this.npm).split(""); //membuat long npm menjadi array
        //Slicing array menjadi bagian kode jurusan
        String[] arrofJurusan = getSlice(arrofNpm, 2, 4);
        
        //Set array untuk setiap kode jurusan
        String[] ilmuKomputer = {"0","1"};
        String[] sistemInformasi = {"0","2"};
                
                
        //Conditional mengecek apakah kode yang didapatkan sama dengan tiap kode jurusan yang ada
        if (Arrays.equals(arrofJurusan,ilmuKomputer)){
            this.jurusan =  "Ilmu Komputer";
        } else if (Arrays.equals(arrofJurusan,sistemInformasi)){
            this.jurusan = "Sistem Informasi";
        }
        
        return this.jurusan;
    }
    
    public String[] getMasalahIRS(){
        return this.masalahIRS;
    }

    public int getTotalSKS(){
        return this.totalSKS;
    }
    
    //Method addMatkul
    public void addMatkul(MataKuliah matKul){
        for (int i = 0; i < mataKuliah.length; i ++){
            //Apabila di dalam daftar matkul terdapat matkul yang inin ditambah
            if (Arrays.asList(mataKuliah).contains(matKul)){
                System.out.println("[DITOLAK] " + matKul.getNama() + " telah diambil sebelumnya");
                break;
            } else if (matKul.getNumOfMahasiswa() == matKul.getKapasitas()){ //Apabila kapasitas matkul penuh
                System.out.println("[DITOLAK] " + matKul.getNama() + " telah penuh kapasitasnya");
                break;
            } else if (numOfMatkul == 10){ //Apabila matkul yang diambil telah 10
                System.out.println("[DITOLAK] Maksimal mata kuliah yang diambil hanya 10");
                break;
            } else if (mataKuliah[i] == null){ //Menambahkan matkul pada array mataKuliah
                mataKuliah[i] = matKul;
                numOfMatkul++;         
                totalSKS += mataKuliah[i].getSks();   //Menjumlahkan sks   
                break;
            }
        }
    }

    public void dropMatkul(MataKuliah matKul){
        //Mengecek apakah nama matkul sudah terdapat di dalam daftar matkul atau belum
        boolean check = Arrays.asList(mataKuliah).contains(matKul);
        //Apabila matkul pernah diambil
        if (check){
            for (int i = 0; i < mataKuliah.length; i++){
                if (mataKuliah[i] == null){
                    continue;
                } else if (mataKuliah[i] == matKul){
                    //Mengurangi jumlah sks total
                    totalSKS -= mataKuliah[i].getSks();
                    //Menghilangkan masalah (apabila ada)
                    this.removeMasalah(matKul, jurusan);
                    //Mengubah matkul menjadi null
                    mataKuliah[i] = null;
                    //Mengurangi jumlah matkul yang diambil mahasiswa
                    numOfMatkul--;
                }
            }
        } else { //Validasi apabila matkul belum pernah diambil
            System.out.println("[DITOLAK] " + matKul.getNama() + " belum pernah diambil");
        }
    }

    //Method untuk menghilangkan masalah jurusan pada masalah IRS 
    public void removeMasalah(MataKuliah matkul, String jurusanMahasiswa){
        //Apabila di dalam masalah irs terdapat masalah
        if(Arrays.asList(masalahIRS).contains("Mata kuliah " + matkul + " tidak dapat diambil di jurusan " + jurusanMahasiswa)){
            for (int j = 0; j < masalahIRS.length; j++){
                if (masalahIRS[j] == null){
                    continue;
                } else if(masalahIRS[j].equals("Mata kuliah " + matkul + " tidak dapat diambil di jurusan " + jurusanMahasiswa)){
                    //Menghilangkan masalah menjadi null
                    masalahIRS[j] = null;
                    numOfMasalah--;
                }
            }
        }
    }

    public void cekIRS(){
        //Conditional apabila totalsks sudah lebih dari 24
        if (totalSKS > 24){
            boolean val = Arrays.asList(masalahIRS).contains("SKS lebih dari 24");
            //Mengecek apakah sudah ada masalah ini di dalam masalah irs sebelumnya
            if(val == false){
                //Apabila yang paling awal null, maka masalah dimasukkan ke situ
                if (masalahIRS[0] == null){
                    masalahIRS[0] = "SKS lebih dari 24";
                } else { //Jika paling awal tidak null, maka masalah sks lebih dari 24 harus diprioritaskan (pindahkan ke awal)
                    String[] arrCopy = Arrays.copyOf(masalahIRS, masalahIRS.length);
                    for (int i = 1; i< masalahIRS.length; i ++){
                        masalahIRS[0] = "SKS lebih dari 24";
                        masalahIRS[i] = arrCopy[i-1];
                    }
                }
            }   
        } else { //Apabila total sks < 24
            //Conditional apabila di dalam masalahIRS terdapat masalah SKS lebih dari 24
            if(Arrays.asList(masalahIRS).contains("SKS lebih dari 24")){
                //Penghapusan masalah tersebut dari masalahIRS
                for(int i = 0; i < masalahIRS.length; i++){
                    if( masalahIRS[i] == null){
                        continue;
                    } else if (masalahIRS[i].equals("SKS lebih dari 24")){
                        masalahIRS[i] = null;
                    }
                }
            }
        }

        //Mengecek kesesuaian kode matkul dengan jurusannya
        for (MataKuliah i : mataKuliah){
            if (i == null){
                continue;
            }else if(jurusan.equals("Ilmu Komputer")){
                //Conditional apabila ada matkul yang kodenya tidak sesuai sama jurusan mahasiswa
                if(i.getKode().equals("IK") || i.getKode().equals("CS")){
                    continue;
                } else {
                    //Mengecek apakah sudah terdapat masalah ini sebelumnya
                    if (Arrays.asList(masalahIRS).contains("Mata kuliah " + i + " tidak dapat diambil di jurusan IK")){
                        continue;
                    } else {
                        //Memasukkan masalah ke dalam array masalahIRS
                        for(int k = 0; k < masalahIRS.length; k++){
                            if (masalahIRS[k] == null){
                                masalahIRS[k] = ("Mata kuliah " + i + " tidak dapat diambil di jurusan IK");
                                numOfMasalah++;
                                break;
                            }
                        }                   
                    }
                }
                
            } else if (jurusan.equals("Sistem Informasi")){
                //Conditional apabila ada matkul yang kodenya tidak sesuai sama jurusan mahasiswa
                if(i.getKode().equals("SI") || i.getKode().equals("CS")){
                    continue;
                } else{
                    //Mengecek apakah sudah terdapat masalah ini sebelumnya
                    if(Arrays.asList(masalahIRS).contains("Mata kuliah " + i + " tidak dapat diambil di jurusan SI")){
                        continue;
                    } else{
                        //Memasukkan masalah ke dalam array masalahIRS
                        for(int k = 0; k < masalahIRS.length; k++){
                            if (masalahIRS[k] == null){
                                masalahIRS[k] = ("Mata kuliah " + i + " tidak dapat diambil di jurusan SI");
                                numOfMasalah++;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
    
    //Membuat method slicing
    public static String[] getSlice(String[] arr, int startIndex,int endIndex){
        //Membuat copy dari array yang asli dengan range panjang sesuai yang mau dipotong
        String[] slice = Arrays.copyOfRange(arr, startIndex,endIndex);
        return slice;
    }

    public String toString() {
        //Mengembalikan nama mahasiswa
        return this.nama;
    }

   

}
