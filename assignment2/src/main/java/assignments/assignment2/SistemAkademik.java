package assignments.assignment2;

import java.util.Scanner;

public class SistemAkademik {
    private static final int ADD_MATKUL = 1;
    private static final int DROP_MATKUL = 2;
    private static final int RINGKASAN_MAHASISWA = 3;
    private static final int RINGKASAN_MATAKULIAH = 4;
    private static final int KELUAR = 5;
    private static Mahasiswa[] daftarMahasiswa = new Mahasiswa[100];
    private static MataKuliah[] daftarMataKuliah = new MataKuliah[100];
    
    private Scanner input = new Scanner(System.in);

    //Method untuk mencari mahasiswa berdasarkan npmnya
    private Mahasiswa getMahasiswa(long npm) {
        //iterasi untuk setiap mahasiswa di daftarMahasiswa
        for (Mahasiswa i : daftarMahasiswa){
            //Apabila npmnya sesuai dengan yg dicari
            if (i.getNpm() == npm){
                return i; //mengembalikan objek mahasiswa
            }
        }return null;
    }

    //Method untuk mencari matkul berdasarkan namanya
    private MataKuliah getMataKuliah(String namaMataKuliah) {
        //Iterasi untuk setiap matkul di daftarMatakuliah
        for (MataKuliah i : daftarMataKuliah){
            //Apabila nama dari matkul sama dengan nama yang dicari
            if (i.getNama().equals(namaMataKuliah)){
                return i; //Akan mengembalikan objek matkul
            }
        } return null;
    }

    private void addMatkul(){
        System.out.println("\n--------------------------ADD MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan ADD MATKUL : ");
        long npm = Long.parseLong(input.nextLine());
        //Mencari mahasiswa berdasarkan NPM nya
        Mahasiswa mhs = getMahasiswa(npm);
        //Conditional apabila mahasiswa tidak ditemukan
        if (mhs == null){
            System.out.println("NPM Salah! Mahasiswa tidak ditemukan");
        } else {
            System.out.print("Banyaknya Matkul yang Ditambah: ");
            int banyakMatkul = Integer.parseInt(input.nextLine());
            System.out.println("Masukkan nama matkul yang ditambah");
            for(int i=0; i<banyakMatkul; i++){
                System.out.print("Nama matakuliah " + i+1 + " : ");
                String namaMataKuliah = input.nextLine();
                //Mencari matkul menggunakan method getMataKuliah
                MataKuliah matkul = getMataKuliah(namaMataKuliah);
                //Menambahkan matkul ke array mahasiswa dan sebaliknya
                mhs.addMatkul(matkul); 
                matkul.addMahasiswa(mhs);
            }
        System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
        }
    }

    private void dropMatkul(){
        System.out.println("\n--------------------------DROP MATKUL--------------------------\n");

        System.out.print("Masukkan NPM Mahasiswa yang akan melakukan DROP MATKUL : ");
        long npm = Long.parseLong(input.nextLine());

       /* TODO: Implementasikan kode Anda di sini 
        Jangan lupa lakukan validasi apabila mahasiswa belum mengambil mata kuliah*/
        //Mencari mahasiswa berdasarkan NPM-nya
        Mahasiswa mhs = getMahasiswa(npm);

        //Conditional apabila mahasiswa tidak ditemukan
        if (mhs == null){   
            System.out.println("NPM Salah! Mahasiswa tidak ditemukan");
        } else if (mhs.getNumOfMatkul() <= 0){ //Validasi apabila mahasiswa belum mengambil matkul
            System.out.println("[DITOLAK] Belum ada mata kuliah yang diambil.");
        }
        else {
            System.out.print("Banyaknya Matkul yang Di-drop: ");
            int banyakMatkul = Integer.parseInt(input.nextLine());
            System.out.println("Masukkan nama matkul yang di-drop:");
            for(int i=0; i<banyakMatkul; i++){
                System.out.print("Nama matakuliah " + i+1 + " : ");
                String namaMataKuliah = input.nextLine();
                /* TODO: Implementasikan kode Anda di sini */
                //Mencari matkul berdasarkan namanya
                MataKuliah matkul = getMataKuliah(namaMataKuliah);
                //Mengurangi matkul di array mahasiswa dan sebaliknya
                mhs.dropMatkul(matkul); 
                matkul.dropMahasiswa(mhs);
            }
            System.out.println("\nSilakan cek rekap untuk melihat hasil pengecekan IRS.\n");
        }
    }

    private void ringkasanMahasiswa(){
        System.out.print("Masukkan npm mahasiswa yang akan ditunjukkan ringkasannya : ");
        long npm = Long.parseLong(input.nextLine());

        // TODO: Isi sesuai format keluaran
        //Mencetak output sesuai format
        Mahasiswa mhs = getMahasiswa(npm);
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama: " + mhs.getNama());
        System.out.println("NPM: " + npm);
        System.out.println("Jurusan: " + mhs.getJurusan());
        System.out.println("Daftar Mata Kuliah: ");

        /* TODO: Cetak daftar mata kuliah 
        Handle kasus jika belum ada mata kuliah yang diambil*/
        boolean allNull = true;
        for(int i = 0; i < mhs.getMataKuliah().length; i++){
            if (mhs.getMataKuliah()[i] != null){
                allNull = false;
                break;
            } 
        }
        if (allNull){
            System.out.println("Belum ada mata kuliah yang diambil");
        } else {
            int no = 1;
            for(int j = 0; j < mhs.getMataKuliah().length; j++){
                if (mhs.getMataKuliah()[j] == null){
                    continue;
                } else {
                    System.out.println(no + ". " + mhs.getMataKuliah()[j].getNama());
                    no++;
                }
            }
        }
        
        
        System.out.println("Total SKS: " + String.valueOf(mhs.getTotalSKS()));
        
        //Mencetak IRS sesuai format
        System.out.println("Hasil Pengecekan IRS:");
        mhs.cekIRS();
        boolean allNullIrs = true;
        //Looping untuk array masalah IRS pada tiap mahasiswa
        for(int k = 0; k < mhs.getMasalahIRS().length; k++){
            //Conditional, apabila menemukan suatu masalah dalam array
            if (mhs.getMasalahIRS()[k] != null){
                allNullIrs = false; //set var allNullIrs ke false
                break;
            }
        }
        //Conditional apabila semua array masalah adalah null maka IRS tidak bermasalah
        if (allNullIrs){
            System.out.println("IRS tidak bermasalah.");
        } else {
            int no = 1; //set var untuk nomor
            //Looping untuk tiap - tiap masalah
            for(int l = 0; l < mhs.getMasalahIRS().length; l++){
                if (mhs.getMasalahIRS()[l] == null){
                    continue;
                } else {
                    //Mencetak masalah sesuai dengan format
                    System.out.println(no + ". " + mhs.getMasalahIRS()[l]);
                    no ++;
                }
            }
        }
    }

    private void ringkasanMataKuliah(){
        System.out.print("Masukkan nama mata kuliah yang akan ditunjukkan ringkasannya : ");
        String namaMataKuliah = input.nextLine();
        MataKuliah matkul = getMataKuliah(namaMataKuliah);
        //Mencetak Ringkasan matkul sesuai dengan format output
        System.out.println("\n--------------------------RINGKASAN--------------------------\n");
        System.out.println("Nama mata kuliah: " + matkul.getNama());
        System.out.println("Kode: " + matkul.getKode());
        System.out.println("SKS: " + matkul.getSks());
        System.out.println("Jumlah mahasiswa: " + matkul.getNumOfMahasiswa());
        System.out.println("Kapasitas: " + matkul.getKapasitas());
        System.out.println("Daftar mahasiswa yang mengambil mata kuliah ini: ");
       /* TODO: Cetak hasil cek IRS
        Handle kasus jika tidak ada mahasiswa yang mengambil */
        boolean allNull = true; //Set variable allNull
        //Looping untuk array DaftarMahasiswa
        for(int i = 0; i < matkul.getDaftarMahasiswa().length; i++){
            //Apabila menemukan mahasiswa di dalam array
            if(matkul.getDaftarMahasiswa()[i] != null){
                allNull = false; //Set variable allNull menjadi false
                break;
            }
        }
        //Apabila isi dari array kosong semua (null), maka belum ada mahasiswa yang mengambil
        if(allNull){
            System.out.println("Belum ada mahasiswa yang mengambil mata kuliah ini");
        } else {
            //Set variable nomor
            int no = 1;
            //Looping untuk setiap array daftarMahasiswa
            for(int j = 0; j< matkul.getDaftarMahasiswa().length; j++){
                if (matkul.getDaftarMahasiswa()[j] == null){
                    continue;
                } else {
                    //Mencetak sesuai format
                    System.out.println(no + ". " + matkul.getDaftarMahasiswa()[j].getNama());
                    no++;
                }
            }
        }
    }

    private void daftarMenu(){
        int pilihan = 0;
        boolean exit = false;
        while (!exit) {
            System.out.println("\n----------------------------MENU------------------------------\n");
            System.out.println("Silakan pilih menu:");
            System.out.println("1. Add Matkul");
            System.out.println("2. Drop Matkul");
            System.out.println("3. Ringkasan Mahasiswa");
            System.out.println("4. Ringkasan Mata Kuliah");
            System.out.println("5. Keluar");
            System.out.print("\nPilih: ");
            try {
                pilihan = Integer.parseInt(input.nextLine());
            } catch (NumberFormatException e) {
                continue;
            }
            System.out.println();
            if (pilihan == ADD_MATKUL) {
                addMatkul();
            } else if (pilihan == DROP_MATKUL) {
                dropMatkul();
            } else if (pilihan == RINGKASAN_MAHASISWA) {
                ringkasanMahasiswa();
            } else if (pilihan == RINGKASAN_MATAKULIAH) {
                ringkasanMataKuliah();
            } else if (pilihan == KELUAR) {
                System.out.println("Sampai jumpa!");
                exit = true;
            }
        }
    }


    private void run() {
        System.out.println("====================== Sistem Akademik =======================\n");
        System.out.println("Selamat datang di Sistem Akademik Fasilkom!");
        
        System.out.print("Banyaknya Matkul di Fasilkom: ");
        int banyakMatkul = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan matkul yang ditambah");
        System.out.println("format: [Kode Matkul] [Nama Matkul] [SKS] [Kapasitas]");

        for(int i=0; i<banyakMatkul; i++){
            String[] dataMatkul = input.nextLine().split(" ", 4);
            int sks = Integer.parseInt(dataMatkul[2]);
            int kapasitas = Integer.parseInt(dataMatkul[3]);
            //Instance mata kuliah
            MataKuliah matkul = new MataKuliah(dataMatkul[0], dataMatkul[1], sks, kapasitas);
            daftarMataKuliah[i] = matkul; //memasukan matkul ke dalam array
        }

        System.out.print("Banyaknya Mahasiswa di Fasilkom: ");
        int banyakMahasiswa = Integer.parseInt(input.nextLine());
        System.out.println("Masukkan data mahasiswa");
        System.out.println("format: [Nama] [NPM]");

        for(int i=0; i<banyakMahasiswa; i++){
            String[] dataMahasiswa = input.nextLine().split(" ", 2);
            long npm = Long.parseLong(dataMahasiswa[1]);
            //Instance mahasiswa
            Mahasiswa mhs = new Mahasiswa(dataMahasiswa[0], npm);
            daftarMahasiswa[i] = mhs; //memasukkkan mahasiswa ke dalam array
        }

        daftarMenu();
        input.close();
    }

    public static void main(String[] args) {
        SistemAkademik program = new SistemAkademik();
        program.run();
    }


    
}
