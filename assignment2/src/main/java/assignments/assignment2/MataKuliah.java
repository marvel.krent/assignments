package assignments.assignment2;
import java.util.Arrays;

public class MataKuliah {
    private String kode;
    private String nama;
    private int sks;
    private int kapasitas;
    private Mahasiswa[] daftarMahasiswa;
    private int numOfMahasiswa;

    //Constructor class MataKuliah
    public MataKuliah(String kode, String nama, int sks, int kapasitas){
        this.kode = kode;
        this.nama = nama;
        this.sks = sks;
        this.kapasitas = kapasitas;
        this.daftarMahasiswa = new Mahasiswa[kapasitas];
    }

    //method getter
    public String getKode(){
        return this.kode;
    }

    public int getSks(){
        return this.sks;
    }

    public String getNama(){
        return this.nama;
    }

    public int getKapasitas(){
        return this.kapasitas;
    }

    public int getNumOfMahasiswa(){
        return this.numOfMahasiswa;
    }

    public Mahasiswa[] getDaftarMahasiswa(){
        return this.daftarMahasiswa;
    }

    //Method addMahasiswa
    public void addMahasiswa(Mahasiswa mahasiswa) {
        //Mengecek apakah di dalam daftar mahasiswa sudah terdapat mahasiswa 
        if (Arrays.asList(daftarMahasiswa).contains(mahasiswa)){} 
        //Apabila belum akan memasukkan mahasiswa ke dalam daftarMahasiswa
        else {
            for (int i = 0; i < daftarMahasiswa.length; i ++){
                if (daftarMahasiswa[i] == null){
                    daftarMahasiswa[i] = mahasiswa;
                    numOfMahasiswa++;    //Menambahkan jumlah mahasiswa          
                    break;
                } 
            }
        }
    }

    public void dropMahasiswa(Mahasiswa mahasiswa) {
        //Mengecek apakah mahasiswa terdapat di dalam daftar atau tidak
        boolean check = Arrays.asList(daftarMahasiswa).contains(mahasiswa);
        //Apabila terdapat mahasiswa di dalam daftar
        if(check){
            //menghilangkah mahasiswa dari daftar
            for (int i = 0; i < daftarMahasiswa.length; i++){
                if (daftarMahasiswa[i] == null){    
                    continue;
                } else if (daftarMahasiswa[i] == mahasiswa){
                    daftarMahasiswa[i] = null;
                    numOfMahasiswa--; //Mengurangi jumlah mahasiswa
                }
            }
        }
    }

    public String toString() {
        //Mengembalikan nama matkul
        return this.nama;
    }

}
